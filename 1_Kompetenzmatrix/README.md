# Kompetenzmatrix - Modul 123

## Handlungsziele und typische Handlungssituationen
#### 1. Konfiguration aufgrund von Vorgaben definieren

Der Lernende plant aufgrund vorhandener Netzwerk- und Firmendokumentation für ein KMU die Konfiguration einzelner Serverdienste (Verzeichnisdienst, DHCP, DNS, File, Print) und dokumentiert seine Planung nachvollziehbar.

#### 2. Serverdienste gemäss definierter Vorgaben installieren und konfigurieren

Der Lernende installiert anhand von Anleitungen einzelne Serverdienste und konfiguriert diese gemäss der erstellten Vorgaben. Nicht benötigte Funktionen und Dienste werden deaktiviert. Die Konfiguration wird fachgerecht dokumentiert.

#### 3. Notwendige Anpassungen an Clients vornehmen

Nach der Bereitstellung der Serverdienste passt der Lernende die Clients so an, dass die Funktion für die Benutzenden gewährleistet ist.

#### 4. Grundlegende Sicherheitsmassnahmen und Zugriffschutz implementieren

Der Lernende sichert die Betriebssysteme, Konfigurationen und Daten vor unberechtigtem Zugriff, bzw. Manipulation (Zugriffsschutz), gemäss den Vorgaben im Betrieb und dokumentiert diese Massnahmen.

#### 5. Verfügbarkeit und Funktionalität testen und protokollieren

Der Lernende testet die Funktionalität einzelner Serverdienste gemäss der vorgegebenen Spezifikationen, Grenzwerten und Verfügbarkeitsanforderungen. Die Ergebnisse werden nachvollziehbar protokolliert. 

#### 6. Betriebsdokumentation zu Serverdienst erstellen

Der Lernende kann die erstellten Anforderungen, Konfigurationen und Protokolle zu einer fachgerechten und nachvollziehbaren Dokumentation vervollständigen.

## Matrix

| Kompetenzband:               | HZ   | Grundlagen                                                   | Fortgeschritten                                              | Erweitert                                                    |
| ---------------------------- | ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Hardware                     | 1    | A1G: Ich kann wesentliche Merkmale von Serverhardware aufzählen. | A1F: Ich kann den Einfluss von einzelnen Hardwarekomponenten auf die Performance erklären. | A1E: Ich kann für einzelne Serverdienste sinnvolle Hardwareausrüstung empfehlen. |
| Funktion                     | 1    | B1G: Ich kann das Prinzip von Serverdiensten (Server-Client) erklären. | B1F: Ich kann die Funktion der einzelnen Serverdienste (DNS, DHCP, File, Print, Verzeichnisdienst) im Netzwerk erläutern. | B1E: Ich kann, anhand betrieblicher Bedürfnisse, spezifische Funktionen von Serverdiensten zu erkennen und auszuwählen. |
|                              | 1    | B2G: Ich kann die per DHCP einem Client zugewiesenen Angaben erklären. | B2F: Ich kann, die mit DHCP zugewiesenen Werte, deren Grenzen und den Ablauf einer Zuweisung zu erklären. | B2E: Ich kann die zugewiesenen DHCP-Parameter, den Ablauf einer Zuweisung und den Einfluss zusätzlicher Parameter (Release-Time, ...) erklären und erläutern, wann eine statische Zuweisung von IP-Adressen sinnvoll ist. |
|                              | 1    | B3G: Ich kann das Grundprinzip der DNS-Namensauflösung erklären. | B3F: Ich kann das Prinzip und den Ablauf einer DNS-Namensauflösung erläutern und kann Tools einsetzen, um die Funktionalität zu überprüfen. | B3E: Ich kann die DNS-Auflösung erklären, die Funktionalität überprüfen und über die Hosts-Datei die DNS-Auflösung beeinflussen. |
|                              | 1    | B4G: Ich kann das Prinzip des Bereitstellens von Daten über Netzwerkfreigaben erklären. | B4F: Ich kann Kriterien (Datensicherheit, Zugriffsberechtigungen, Bezeichnungen) für die gemeinsame Nutzung von Daten über Netzwerkfreigaben erläutern. | B4E: Ich kann die Funktion von Netzwerkfreigaben erläutern, kann zusätzliche Möglichkeiten (versteckte Freigaben, Werkzeuge für Tests)  und Berechtigungskonzepte (Dateisystem, Freigabe) planen. |
|                              | 1    | B5G: Ich kann Vor- und Nachteile von Druckerservern gegenüber dem Direktdruck erläutern. | B5F: Ich kann die Einstellungen eines Druckerservers und seine Funktionen in einem Netzwerk erklären und beeinflussen. | B5E: Ich kann die Funktion eines Druckerservers erklären und die Bereitstellung von Druckertreibern planen. |
| Konfiguration                | 1,2  | C1G: Ich kann verschiedene Konfigurationsarten auflisten.    | C1F: Ich kann bestehende Konfigurationen interpretieren und anpassen. | C1E: Ich kann Konfigurationen auf verschiedenen Systemen gemäss Vorbereitung erstellen. |
|                              | 1,2  | C2G: ich kann eine vorgegebene Konfiguration auf dem System umsetzen. | C2F: Ich kann Vorgaben eigenständig in Konfigurationen übertragen und auf dem System umsetzen. | C2E: Ich kann spezielle Anwendungen eigenständig in Konfigurationen übertragen und auf dem System umsetzen. |
| Installation                 | 2    | D1G: Ich kann Serverdienste/-rollen installieren.            | D1F: Ich kenne unterschiedliche Möglichkeiten Dienste zu installieren und zu aktivieren. | D1E: Ich kann Serverdienste aus unterschiedlichen Quellen vergleichen, Vor- und Nachteile auswerten und einen passenden Dienst installieren. |
| Prüfen                       | 2    | E1G: Ich kann feststellen, ob der Serverdienst korrekt startet. | E1F: Ich kann die grundlegende Funktion eines Serverdienstes überprüfen und kenne dazu verschiedene Werkzeuge (Befehle, Schnittstellen, Log-Dateien). | E1E: Ich kann Fehlfunktionen bei Serverdiensten mit einem Repertoire verschiedener Werkzeuge (Befehle, Schnittstellen, Log-Dateien) erkennen und daraus Massnahmen abzuleiten. |
| Client-Anpassung             | 3    | F1G: Ich kann auf dem Client die entsprechende Funktion aktivieren/einrichten. | F1F: Ich kann auf dem Client die Funktion aktivieren und die Funktionalität überprüfen. | F1E: Ich kann auf dem Client die Funktion einrichten, überprüfen und zusätzliche Ereignisse für Tests auslösen. |
| Zugriffsschutz               | 4    | G1G: Ich kann verschiedene Arten von Zugriffsschutz im Netzwerk und auf dem Dateisystem erklären. | G1F: Ich kann Zugriffsschutz auf verschiedenen Systemen umsetzen. | G1E: Ich kann einen kombinierten Zugriffschutz über Benutzer- und Gruppenrechte erreichen (z. B. AGDLP-Prinzip). |
| Sicherheit von Software      | 4    | H1G: Ich kann verschiedene Möglichkeiten aufzählen, Systeme und Software zu warten. | H1F:Ich kann Systeme und Software warten und kann Sicherheitsaspekte zur Absicherung aufzählen. | H1E: Ich kann Systeme und Software fachgerecht betreuen und kann dabei wesentliche Sicherheitsaspekte (Benutzerauthentifizierung, Zugriffschutz auf Konfigurationen und Dienste) umsetzen. |
|                              | 4    | I1G: Ich kann aktive Dienste auf einem System erkennen.      | I1F: Ich kann aktive Dienste auf einem System erkennen und nicht benötigte deaktivieren. | I1E: Ich kann aktive Dienste und deren offenen Schnittstellen erkennen und nicht benötigte Dienste deaktivieren, bzw. Schnittstellen schliessen. |
| Funktionstest<br />Protokoll | 5    | J1G: Ich kann mit einfachen Tests die Funktion eines Dienstes prüfen. | J1F:  Ich kann mit gut gewählten Testfällen die Funktion eines Dienstes prüfen und notwendige Massnahmen umsetzen. | J1E: Ich kann mit gut gewählten Testfällen die Funktion und Grenzen des Dienstes nachvollziehbar aufzeigen und notwendige Massnahmen umsetzen. |
| Betriebsdokumentation        | 6    | K1G: Ich kann wesentliche Aspekte eines Serverdienstes dokumentieren. | K1F: Ich kann eine vollständige Dokumentation von Serverdiensten erstellen. | K1E: Ich kann eine vollständige und nachvollziehbare Dokumentation von Serverdiensten erstellen, die Konfiguration, Funktionstests und Planung bzw. Überlegungen dazu abbildet. |

## Kompetenzstufe

### Grundlagen | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.0.*

### Fortgeschritten | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.5*

### Erweitert | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*

## Fragekatalog

Link zum [Fragekatalog](Fragenkatalog.md)