# Umsetzungsvorschlag - Modul 123


## Lektionenplan

| Anzahl Lektionen | Themen                                                       | Kompetenzen             | Tools                                                        |
| ---------------- | ------------------------------------------------------------ | ----------------------- | ------------------------------------------------------------ |
| 2                | **Einführung in Server-Themen** <br />- Client-Server<br />- Serverhardware | A1, B1                  | Internet Recherche, Hersteller-Websites                      |
| 3                | **Grundlagen zu und Funktionsweise von Diensten im Netzwerk**<br />anhand von konkreten Beispielen und/oder Simulationen <br />- DNS<br />- DHCP<br />- Freigaben, CIFS & SMB<br />- Print | B                       | virtuelle Maschinen, [Filius](https://www.lernsoftware-filius.de/), [Packet Tracer](https://www.netacad.com/courses/packet-tracer) |
| 8                | **Praxisauftrag zu Freigaben** <br />- Windows (SMB/CIFS) und <br />- Linux (SAMBA) <br />inkl. Berechtigungen auf verschiedenen Dateisystemen. Abschluss mit Testprotokoll | B4, C, D, E, F, G, J    | Betriebssysteme, virtuelle Maschinen                         |
| 5                | Einführung  in **Dienst DHCP** <br />- Einrichten Umgebung <br />- Auftrag DHCP (Windows oder Linux) | B2, C, D, E, F, G, J    | Betriebssysteme, virtuelle Maschinen                         |
| 2                | - Einführung in **Wireshark**<br />- Abschluss DHCP-Auftrag mit **Testprotokoll** und Wiresharkauszug | E, J                    | [Wireshark](https://www.wireshark.org/)                      |
| 2                | Betriebsdokumentation und Abnahmeprotokoll für Serverdienst aufzeigen und Auftrag dazu (zu beliebigem Dienst) | K                       |                                                              |
| 4                | Einführung in Dienst DNS, Einrichten und Auftrag (Windows oder Linux) mit Testprotokoll | B3, C, D, E, E, F, G, J | Betriebssysteme                                              |
| 4                | Logfiles analysieren, Sicheheitsbetrachtungen und -anpassungen an vorhandenen Diensten | C, G, I, J              | Systemtools für Netzwerkanalyse                              |
| 4                | zusätzliche Dienste zur Vertiefung (z. B. Nextcloud, piHole) | B, C, D, E, F, G, J     |                                                              |
| 4                | Reserviert Kompetenznachweise                                |                         |                                                              |
| 2                | Reserve                                                      |                         |                                                              |
