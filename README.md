# 123 Serverdienste in Betrieb nehmen



## Version der Modulidentifikation

Version 2 - 2021-02-11 - [Modulidentifikation](https://mbk.ict-berufsbildung.ch/modul/fe74c9da-716c-eb11-b0b1-000d3a830b2b)

## Kurzbeschreibung des Moduls gemäss Modulidentifikation

Verschiedene Serverdienste für den lokalen Netzwerkbetrieb nach Vorgaben installieren und konfigurieren. Funktionalität der Serverdienste überprüfen.

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

- aus Modul 117 IP-Adressierung

- Modul 187 ICT-Arbeitsplatz mit Betriebssystem in Betrieb nehmen

### Nachfolgende Module

- Modul 169 Dienste mit Containern bereitstellen

- Modul 188 Services betreiben, warten und überwachen  (ÜK)

## Kompetenzmatrix

[Kompetenzmatrix](1_Kompetenzmatrix) zu den Handlungszielen in drei Abstufungen.

## Umsetzungsvorschlag

[Umsetzungsvorschlag](3_Umsetzungsvorschlag) mit möglichem Modulablauf.

## Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen.
